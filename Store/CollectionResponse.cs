﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eju.CC.Common;

namespace bgm.tv.Store
{
    /// <summary>
    /// 获取目前追番信息(详细)
    /// </summary>
    public class CollectionResponse
    {
        public CollectionCurrentStatus status { get; set; }
        public int rating { get; set; }
        public string comment { get; set; }
        public string[] tag { get; set; }
        public int ep_status { get; set; }
        public int lasttouch { get; set; }
        public User user { get; set; }
    }

    /// <summary>
    /// 获取目前追番信息 结果
    /// </summary>
    public class CollectionItem
    {
        public string name { get; set; }
        public int ep_status { get; set; }
        public string lasttouch { get; set; }
        public SimpleSubject subject { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", EnumHelper<BangumiType>.GetDisplayValue(subject.SubjectType), name);
        }
    }
    
}
