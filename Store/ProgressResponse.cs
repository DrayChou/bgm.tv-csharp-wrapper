﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace bgm.tv.Store
{

    /// <summary>
    /// 获取追番进度信息
    ///!(需要认证信息
    ///获取某一个番所有集数的信息，以及用户的追番信息。
    /// </summary>
    public class ProgressResponse
    {
        [JsonProperty("subject_id")]
        public int SubjectId { get; set; }

        public ProgressEp[] eps { get; set; }
    }
}
