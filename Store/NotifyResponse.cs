﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgm.tv.Store
{
    /// <summary>
    /// 通知结果
    /// </summary>
    public class NotifyResponse:BaseBgmResponse
    {
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}
