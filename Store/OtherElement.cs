﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace bgm.tv.Store
{
    public class Rating
    {
        public int total { get; set; }
        public StarInfo count { get; set; }
        public float score { get; set; }
    }

    /// <summary>
    /// 评分
    /// </summary>
    public class StarInfo
    {
        [JsonProperty("1")]
        public int Star_1 { get; set; }
        [JsonProperty("2")]
        public int Star_2 { get; set; }
        [JsonProperty("3")]
        public int Star_3 { get; set; }
        [JsonProperty("4")]
        public int Star_4 { get; set; }
        [JsonProperty("5")]
        public int Star_5 { get; set; }
        [JsonProperty("6")]
        public int Star_6 { get; set; }
        [JsonProperty("7")]
        public int Star_7 { get; set; }
        [JsonProperty("8")]
        public int Star_8 { get; set; }
        [JsonProperty("9")]
        public int Star_9 { get; set; }
        [JsonProperty("10")]
        public int Star_10 { get; set; }
    }

    public class Images
    {
        public string large { get; set; }
        public string common { get; set; }
        public string medium { get; set; }
        public string small { get; set; }
        public string grid { get; set; }
    }

    /// <summary>
    /// 此番组全站统计
    /// </summary>
    public class Collection
    {
        public int wish { get; set; }
        public int collect { get; set; }
        public int doing { get; set; }
        public int on_hold { get; set; }
        public int dropped { get; set; }
    }

    /// <summary>
    /// 整个番组的观看状态
    /// </summary>
    public class CollectionCurrentStatus
    {
        public BangumiCollectionStatus id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
    }

    /// <summary>
    /// 番组当前集数状态
    /// </summary>
    public class BangumiEpDetail
    {
        [JsonProperty("id")]
        public BangumiEpStatus StatusFlag { get; set; }
        public string css_name { get; set; }
        public string url_name { get; set; }
        public string cn_name { get; set; }
    }


    public class Crt
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string name_cn { get; set; }
        public string role_name { get; set; }
        public Images images { get; set; }
        public int comment { get; set; }
        public int collects { get; set; }
        public Info info { get; set; }
        public Actor[] actors { get; set; }
    }

    public class Info
    {
        public Alias alias { get; set; }
        public string birth { get; set; }
        [JsonProperty("官网")]
        public string Homepage { get; set; }
        public string name_cn { get; set; }
        [JsonProperty("网址")]
        public string WebAddr { get; set; }
        public string gender { get; set; }
        public object source { get; set; }
    }

    public class Alias
    {
        public string en { get; set; }
        public string jp { get; set; }
        [JsonProperty("原名")]
        public string originName { get; set; }
        public string romaji { get; set; }
        public string nick { get; set; }
        public string kana { get; set; }
    }

    public class Actor
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public Images images { get; set; }
    }


    public class Staff
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string name_cn { get; set; }
        public string role_name { get; set; }
        public Images images { get; set; }
        public int comment { get; set; }
        public int collects { get; set; }
        public Info info { get; set; }
        public string[] jobs { get; set; }
    }

    public class Topic
    {
        public int id { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public int main_id { get; set; }
        public int timestamp { get; set; }
        public int lastpost { get; set; }
        public int replies { get; set; }
        public User user { get; set; }
    }
    public class Avatar
    {
        public string large { get; set; }
        public string medium { get; set; }
        public string small { get; set; }
    }

    public class Blog
    {
        public int id { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string summary { get; set; }
        public string image { get; set; }
        public int replies { get; set; }
        public int timestamp { get; set; }
        public string dateline { get; set; }
        public User user { get; set; }
    }
}
