﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace bgm.tv.Store
{

    /// <summary>
    /// 获取番组/专题信息
    /// </summary>
    public class SubjectResponse : BaseBgmResponse
    {
        public int id { get; set; }
        public string url { get; set; }
        [JsonProperty("type")]
        public BangumiType SubjectType { get; set; }
        public string name { get; set; }
        public string name_cn { get; set; }
        public string summary { get; set; }
        public string air_date { get; set; }
        public int air_weekday { get; set; }
        public Rating rating { get; set; }
        public int rank { get; set; }
        public Images images { get; set; }
        public Collection collection { get; set; }

        //public Crt[] crt { get; set; }
        //public Staff[] staff { get; set; }
        //public Topic[] topic { get; set; }
        //public Blog[] blog { get; set; }

    }

    /// <summary>
    /// 获取番组/专题信息 responseGroup=simple
    /// </summary>
    public class SimpleSubject : SubjectResponse
    {
        public int eps { get; set; }
    }

    /// <summary>
    /// 获取番组/专题信息 responseGroup=large
    /// </summary>
    public class LargeSubject : SubjectResponse
    {
        public SubjectEp[] eps { get; set; }

    }

}
