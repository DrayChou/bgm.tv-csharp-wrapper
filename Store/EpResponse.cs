﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgm.tv.Store
{
    public class EpResponse
    {
        public int id { get; set; }
        public string url { get; set; }
        public int type { get; set; }
        public int sort { get; set; }
        public string name { get; set; }
        public string name_cn { get; set; }
        public string duration { get; set; }
        public string airdate { get; set; }
        public int comment { get; set; }
        public string desc { get; set; }
    }

    public class SubjectEp : EpResponse
    {
        public string status { get; set; }
    }

    public class ProgressEp : EpResponse
    {
        public BangumiEpDetail status { get; set; }
    }
}
