﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgm.tv.Store
{

    public class BaseBgmResponse
    {
        //public string request { get; set; }
        public int? code { get; set; }
        public string error { get; set; }

        public bool GetResult()
        {
            return code == null || code == 200;
        }
    }

}
