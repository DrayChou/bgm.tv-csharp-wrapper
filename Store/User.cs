﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace bgm.tv.Store
{
    public class User : BaseBgmResponse
    {
        public int id { get; set; }
        public string url { get; set; }
        public string username { get; set; }
        public string nickname { get; set; }
        public Avatar avatar { get; set; }
        public object sign { get; set; }
        public string auth { get; set; }
        public string auth_encode { get; set; }
    }


}

