﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Eju.CC.Common
{
    /// <summary>
    ///     枚举助手类
    /// </summary>
    /// <typeparam name="T">枚举类型</typeparam>
    public static class EnumHelper<T>
    {

        /// <summary>
        ///     获取枚举的值列表
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IList<T> GetValues(Enum value)
        {
            var enumValues = new List<T>();

            foreach (var fi in value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                enumValues.Add((T) Enum.Parse(value.GetType(), fi.Name, false));
            }
            return enumValues;
        }

        /// <summary>
        ///     转换成枚举
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Parse(string value)
        {
            try
            {
                return (T) Enum.Parse(typeof (T), value, true);
            }
            catch (Exception exp)
            {
            }
            return default(T);
        }

        /// <summary>
        ///     尝试转换
        /// </summary>
        /// <param name="value"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out T val)
        {
            try
            {
                val = Parse(value);
                return true;
            }
            catch
            {
                val = default(T);
                return false;
            }
        }

        /// <summary>
        ///     获取枚举名称列表
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IList<string> GetNames(Enum value)
        {
            return value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
        }

        /// <summary>
        ///     获取枚举显示值列表
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IList<string> GetDisplayValues(Enum value)
        {
            return GetNames(value).Select(obj => GetDisplayValue(Parse(obj))).ToList();
        }

        /// <summary>
        ///     获取枚举显示值,需要指定Description的Attribute
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplayValue(T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null)
            {
                return string.Empty;
            }
            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof (DescriptionAttribute), false) as DescriptionAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : value.ToString();
        }


        /// <summary>
        ///     获取枚举显示值
        /// </summary>
        /// <param name="valueString"></param>
        /// <returns></returns>
        public static string GetDisplayValue(string valueString)
        {
            var value = default(T);
            TryParse(valueString, out value);

            var fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null)
            {
                return string.Empty;
            }
            var descriptionAttributes = fieldInfo.GetCustomAttributes(
                typeof (DescriptionAttribute), false) as DescriptionAttribute[];

            if (descriptionAttributes == null) return string.Empty;
            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : value.ToString();
        }
    }
}