﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using bgm.tv.Store;

namespace bgm.tv
{
    public class BgmtvClient
    {
        private User _authedUser = null;
        private const string AppName = "onAir";
        private const string Build = "201209121322";
        private const string ApiBase = "https://api.bgm.tv";

        private string BaseParam
        {
            get
            {
                var str = new StringBuilder();
                str.AppendFormat("&source={0}&sysbuild={1}", AppName, Build);
                return str.ToString();
            }
        }

        private string AuthedParam
        {
            get
            {
                if (_authedUser == null)
                {
                    throw new Exception("Need Login");
                }
                var str = new StringBuilder();
                str.AppendFormat("&sysuid={0}", _authedUser.id);
                str.AppendFormat("&sysusername={0}", _authedUser.id);
                str.AppendFormat("&auth={0}", _authedUser.auth);
                return str.ToString();
            }
        }

        private static string MakeRequest(string url, string method = "post")
        {
            var req = WebRequest.Create(url);
            req.Method = method;
            var resp = req.GetResponse();
            try
            {
                using (var rs = new StreamReader(resp.GetResponseStream()))
                {
                    return rs.ReadToEnd();
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Login(string userEmail, string password)
        {
            var url = string.Format("{0}/auth?source=onAir", ApiBase);
            var wc = new WebClient();
            var nvc = new NameValueCollection();
            nvc.Add("username", userEmail);
            nvc.Add("password", password);
            var bResp = wc.UploadValues(url, nvc);
            var respStr = Encoding.UTF8.GetString(bResp);
            var ret = respStr.ToJsonObject<User>();
            _authedUser = ret;
            return ret.code == null;
        }

        public NotifyResponse GetNotify()
        {
            // /notify/count
            var url = string.Format("{0}/notify/count?rand={1}{2}{3}"
                , ApiBase, DateTime.Now.Ticks, BaseParam, AuthedParam);
            var ret = MakeRequest(url);
            return ret.ToJsonObject<NotifyResponse>();
        }

        public SubjectResponse GetBangumi(int bgmSubjectId, bool includeDetail = false)
        {
            // /subject/163046?responseGroup=simple
            var grp = "simple";
            if (includeDetail)
            {
                grp = "large";
            }
            var url = string.Format("{0}/subject/{1}?responseGroup={2}"
                , ApiBase, bgmSubjectId, grp);
            var ret = MakeRequest(url, "get");
            try
            {
                if (includeDetail)
                {
                    return ret.ToJsonObject<LargeSubject>();
                }
                return ret.ToJsonObject<SimpleSubject>();
            }
            catch (Exception exp)
            {
                return new SubjectResponse();
            }
        }

        public List<CollectionItem> GetBangumiCollection()
        {
            // /user/65855/collection?sysuid=65855&sysusername=65855&cat=watching
            // &source=onAir&rand=0%2E020963165443390608&sysbuild=201209121322&auth=-
            var url = string.Format("{0}/user/{1}/collection?sysuid={1}&sysusername={1}&cat=watching{2}{3}"
                , ApiBase, _authedUser.id, BaseParam, AuthedParam);
            var ret = MakeRequest(url);
            return ret.ToJsonObject<List<CollectionItem>>();
        }

        public ProgressResponse GetBangumiProgress(int bgmSubjectId)
        {
            if (_authedUser == null)
            {
                throw new Exception("Need Login");
            }

            // /user/{$UID}/progress?subject_id={$ID}&source=onAir
            var url = string.Format("{0}/user/{1}/progress?subject_id={2}{3}{4}"
                , ApiBase, _authedUser.id, bgmSubjectId, BaseParam, AuthedParam);
            var ret = MakeRequest(url);
            return ret.ToJsonObject<ProgressResponse>();
        }

        /// <summary>
        /// </summary>
        /// <param name="bgmSubjectId"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        public CollectionResponse UpdateCollection(int bgmSubjectId, BangumiCollectionStatus collection)
        {
            var status = string.Empty;
            switch (collection)
            {
                case BangumiCollectionStatus.Do:
                    status = "do";
                    break;
                case BangumiCollectionStatus.OnHold:
                    status = "on_hold";
                    break;
                case BangumiCollectionStatus.Dropped:
                    status = "dropped";
                    break;
                case BangumiCollectionStatus.Wish:
                    status = "wish";
                    break;
                case BangumiCollectionStatus.Collect:
                    status = "collect";
                    break;
            }

            var url = string.Format("{0}/collection/{1}/update?status={2}{3}{4}"
                , ApiBase, bgmSubjectId, status, BaseParam, AuthedParam);
            var wc = new WebClient();
            var nvc = new NameValueCollection();
            nvc.Add("status", status);
            var bResp = wc.UploadValues(url, nvc);
            var respStr = Encoding.UTF8.GetString(bResp);
            return respStr.ToJsonObject<CollectionResponse>();
        }


        public BaseBgmResponse UpdateEp(int epId, BangumiEpStatus ep)
        {
            var epString = string.Empty;
            switch (ep)
            {
                case BangumiEpStatus.Watched:
                    epString = "watched";
                    break;
                case BangumiEpStatus.Drop:
                    epString = "drop";
                    break;
                case BangumiEpStatus.Queue:
                    epString = "queue";
                    break;
            }
            // [POST] /ep/{$EPID}/status/{$STATUS_URL}?source=onAir
            var url = string.Format("{0}/ep/{1}/status/{2}?rand={3}{4}{5}"
                , ApiBase, epId, epString, DateTime.Now.Ticks, BaseParam, AuthedParam);

            var ret = MakeRequest(url);
            return ret.ToJsonObject<BaseBgmResponse>();
        }

        public BaseBgmResponse BatchUpdateEp(List<int> epId)
        {
            if (!epId.Any())
            {
                return new BaseBgmResponse
                {
                    code = 500,
                    error = "ep can not be empty"
                };
            }
            var ids = string.Join(",", epId);
            var url = string.Format("{0}/ep/{1}/status/watched?rand={2}{3}{4}"
                , ApiBase, epId.Last(), DateTime.Now.Ticks, BaseParam, AuthedParam);

            var wc = new WebClient();
            var nvc = new NameValueCollection();
            nvc.Add("ep_id", ids);
            var bResp = wc.UploadValues(url, nvc);
            var respStr = Encoding.UTF8.GetString(bResp);

            return respStr.ToJsonObject<BaseBgmResponse>();
        }
    }
}
