﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bgm.tv
{
    public enum BangumiCollectionStatus
    {
        /// <summary>
        /// do - 在看
        /// </summary>
        [Description("在看")] Do = 0,

        /// <summary>
        /// on_hold - 搁置
        /// </summary>
        [Description("搁置")] OnHold = 1,

        /// <summary>
        /// dropped - 弃番
        /// </summary>
        [Description("弃番")] Dropped = 2,

        /// <summary>
        /// wish - 想看
        /// </summary>
        [Description("想看")] Wish = 3,

        /// <summary>
        /// collect - 看过
        /// </summary>
        [Description("看过")] Collect = 4
    }

    public enum BangumiEpStatus
    {
        /// <summary>
        /// watched - 看过
        /// </summary>
        [Description("看过")] Watched = 2,

        /// <summary>
        /// drop - 弃番
        /// </summary>
        [Description("弃番")] Drop = 3,

        /// <summary>
        /// queue - 想看
        /// </summary>
        [Description("想看")] Queue = 1,

        /// <summary>
        /// remove - 撤销
        /// </summary>
        [Description("撤销")] Remove = 0
    }

    public enum BangumiType
    {
        /// <summary>
        /// 漫画/小说
        /// </summary>
        [Description("漫画/小说")] ComicNovel = 1,

        /// <summary>
        /// 动画/二次元番
        /// </summary>
        [Description("动画/二次元番")] Anime = 2,

        /// <summary>
        /// 音乐
        /// </summary>
        [Description("音乐")] Music = 3,

        /// <summary>
        /// 游戏
        /// </summary>
        [Description("游戏")] Game = 4,

        /// <summary>
        /// 三次元番
        /// </summary>
        [Description("三次元番")] Drama = 6
    }
}
